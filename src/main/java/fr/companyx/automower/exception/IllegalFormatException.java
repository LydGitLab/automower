/**
 * 
 */
package fr.companyx.automower.exception;


/**
 * @author lydiasaighi
 *
 */
public class IllegalFormatException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IllegalFormatException(String message) {
		super(message);
	}

}
