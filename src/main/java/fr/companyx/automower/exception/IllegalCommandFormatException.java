/**
 * 
 */
package fr.companyx.automower.exception;

/**
 * @author lydiasaighi
 *
 */
public class IllegalCommandFormatException extends IllegalFormatException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IllegalCommandFormatException(String message) {
		super(message);
	}

}
