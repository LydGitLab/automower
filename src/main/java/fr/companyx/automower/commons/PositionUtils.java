package fr.companyx.automower.commons;

import fr.companyx.automower.exception.IllegalPositionFormatException;
import fr.companyx.automower.model.Coordinates;
import fr.companyx.automower.model.Orientation;
import fr.companyx.automower.model.Position;

/**
 * @author lydiasaighi
 *
 */
public abstract class PositionUtils {

	private static final String POSITION_FORMAT = "^[0-9] [0-9] [NWSE]";
	private static final String COORDINATE_FORMAT = "[0-9] [0-9]";

	private PositionUtils() {

	}

	public static Position getStartPosition(String position) throws IllegalPositionFormatException {
		isValidPosition(position);

		String[] str = position.split(" ");
		return new Position(Integer.valueOf(str[0]), Integer.valueOf(str[1]), Orientation.getOrientation(str[2]));
	}

	private static void isValidPosition(String position) throws IllegalPositionFormatException {

		if (!position.matches(POSITION_FORMAT)) {
			throw new IllegalPositionFormatException(MessagesErrors.INVALID_POSITION_FORMAT + position);
		}

	}

	public static Coordinates getLimitOfLawn(String limitLawn) throws IllegalPositionFormatException {
		isValidLimitLawn(limitLawn);

		String[] tab = limitLawn.split(" ");
		return new Coordinates(Integer.valueOf(tab[0]), Integer.valueOf(tab[1]));
	}

	private static void isValidLimitLawn(String limitLawn) throws IllegalPositionFormatException {
		if (!limitLawn.matches(COORDINATE_FORMAT)) {
			throw new IllegalPositionFormatException(MessagesErrors.INVALID_COORDINATE_FORMAT + limitLawn);
		}

	}
}
