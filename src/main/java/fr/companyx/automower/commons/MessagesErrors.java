/**
 * 
 */
package fr.companyx.automower.commons;

/**
 * @author lydiasaighi
 *
 */
public class MessagesErrors {

	public static final String INVALID_COMMAND_FORMAT = "Invalid commands format : ";
	public static final String INVALID_POSITION_FORMAT = "Invalid position format : ";
	public static final String INVALID_COORDINATE_FORMAT = "The upper right corner of the lawnn is invalid : ";

}
