package fr.companyx.automower.model;

/**
 * @author lydiasaighi
 *
 */
public enum Orientation {

	NORD("N"), EST("E"), WEST("W"), SUD("S");

	private String code;

	private Orientation(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	/**
	 * Get Orientation by code
	 * 
	 * @param code
	 * @return
	 */
	public static Orientation getOrientation(String code) {

		for (Orientation orientation : Orientation.values()) {
			if (orientation.getCode().equals(code)) {
				return orientation;
			}
		}
		return null;
	}
}
