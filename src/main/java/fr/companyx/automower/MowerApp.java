package fr.companyx.automower;

import java.io.IOException;
import java.util.List;

import fr.companyx.automower.exception.IllegalFormatException;
import fr.companyx.automower.model.Position;
import fr.companyx.automower.service.MowerService;

/**
 * Auto-Mower Program
 *
 */
public class MowerApp {

	public static void main(String[] args) {

		if (args == null || args.length == 0) {
			throw new IllegalArgumentException("file path is required ! ");
		}

		try {
			List<Position> positions = new MowerService().launch(args[0]);
			positions.forEach(System.out::println);
		} catch (IllegalFormatException | IOException e) {

			System.out.println(e);
		}

	}
}
